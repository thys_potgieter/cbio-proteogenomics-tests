#!/usr/bin/env python4

import shutil
import sequtils
import Bio; from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from io import StringIO
import unittest
import os
from Bio.Seq import Seq, translate
import collections
import tempfile
import time
import mqparse
import os
import shutil
import pickle
import yaml
import pandas as pd
import time
import sys
import imp
try:
        from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
        from yaml import Loader, Dumper

class mq_txt(unittest.TestCase):

    def setUp(self):
        self.config = 'testdata/example_mq/config.yaml'
        #if os.path.exists('testdata/example_mq/txt/analysis/config.yml'):
        #    os.remove('testdata/example_mq/txt/analysis/config.yml')

    def tearDown(self):
        pass

    def test_a_load_tables(self):
        outpath='testdata/example_mq/txt/analysis'
        # initiate a blank run 
        if os.path.exists(outpath): 
            shutil.rmtree(outpath)
        if os.path.exists(outpath + '/config.yaml'): 
            os.remove(outpath + '/config.yaml')

        self.mq_txt = mqparse.mq_txt(self.config)
        
        # make sure a template  design.csv is created
        self.assertEqual(True, os.path.exists('testdata/example_mq/txt/analysis/design.csv'))
        
        # copy an existing, one before restarting the pipeline (simulate a user editing the file according to the experimental design)
        shutil.copyfile('testdata/example_mq/design.csv', 'testdata/example_mq/txt/analysis/design.csv')
        # provide the interproscan and ko annotated files, to save time
        shutil.copyfile('testdata/example_mq/proteins.fasta.tsv', 'testdata/example_mq/txt/analysis/fasta/proteins.fasta.tsv')
        shutil.copyfile('testdata/example_mq/proteingroups.ko.csv', 'testdata/example_mq/txt/analysis/proteingroups.ko.csv')

        #if os.path.exists(outpath + '/config.yaml'): 
        #    os.remove(outpath + '/config.yaml')
        self.mq_txt = mqparse.mq_txt(self.config)
        self.assertEqual(len(self.mq_txt.reference_peptides), 801)
        self.assertEqual(len(self.mq_txt.non_reference_peptides), 200) # Withouth excluding contaminants, but excluding reverse
        self.assertEqual(os.path.exists(outpath + '/gsea/LEVEL_1/summary.txt'), True)
        self.assertEqual(os.path.exists(outpath + '/diff/LEVEL_1/peptide_diff/summary.txt'),True)
        self.assertEqual(os.path.exists(outpath + '/proteins/target_proteins.tsv'),True) 
    
    def test_b_qc(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        with open(self.config) as f:
            config = yaml.load(f.read(), Loader=Loader)
            design=pd.read_csv('testdata/example_mq/design.csv')
        summary = pd.read_csv('testdata/example_mq/txt/summary.txt', sep='\t')
        target_peptides = pd.read_csv('testdata/example_mq/txt/analysis/peptides/target_peptides.txt', sep='\t')
        target_proteins = pd.read_csv('testdata/example_mq/txt/analysis/proteins/target_proteins.tsv', sep='\t')
        mqparse.mq_txt.qc(None, config, design, outpath, summary, target_peptides, target_proteins)
        self.assertTrue(os.path.exists(outpath ))
        self.assertTrue(os.path.exists(outpath + '/msms_identifications.png' ))
        self.assertTrue(os.path.exists(outpath + '/bp/peptide_identifications_all.png' ))
        self.assertTrue(os.path.exists(outpath + '/bp/peptide_identifications_LEVEL_1.png'))
        self.assertTrue(os.path.exists(outpath + '/bp/peptide_identifications_LEVEL_2.png'))
        self.assertTrue(os.path.exists(outpath + '/splom/E2_Name.png'))
    
    def test_b_mq_diff(self):
        exp_design = 'testdata/example_mq/txt/analysis/diff/peptide_experimental_design_LEVEL_1.R'
        infile='testdata/example_mq/txt/analysis/unipept/pept2lca_taxon_sc.csv'
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        mqparse.mq_txt.diff(None, exp_design, infile, outpath)
    
    def test_b_mq_diff_2(self):
        exp_design = 'testdata/example_mq/txt/analysis/diff//peptide_experimental_design_LEVEL_1.R'
        infile='testdata/example_mq/txt/analysis/unipept//pept2lca_genus_sc.csv'
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        mqparse.mq_txt.diff(None, exp_design, infile, outpath)
    def test_b_mq_diff_3(self):
        exp_design = 'testdata/example_mq/txt/analysis/diff//peptide_experimental_design_LEVEL_1.R'
        infile='testdata/example_mq/txt/analysis/unipept//pept2lca_family_sc.csv'
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        mqparse.mq_txt.diff(None, exp_design, infile, outpath)
    
    def test_b_human_variant_peptides(self):
        pass
        #design='testdata/example_mq/txt/analysis/diff/peptide_experimental_design.R'
        #infile='testdata/example_mq/txt/analysis/diff/peptide_normalization/msnbase/normalized.csv'
        #outpath='testdata/example_mq/txt/analysis/testing'
    def test_b_unipept(self):
        unipept_dir = 'testdata/example_mq/txt/analysis/unipept_temp'
        if os.path.exists(unipept_dir):
            shutil.rmtree(unipept_dir)
        os.makedirs(unipept_dir)
        mqparse.mq_txt.unipept(None, unipept_dir)
        self.assertTrue(os.path.exists(unipept_dir + '/pept2lca.txt'))

    def test_b_unipept_agg(self):
        
        unipept_dir = 'testdata/unipept_temp'
        if os.path.exists(unipept_dir):
            shutil.rmtree(unipept_dir)
        shutil.copytree('testdata/unipept', unipept_dir)
        normalized_peptides = pd.read_csv(unipept_dir + '/normalized.csv') 
        unipept = pd.read_csv(unipept_dir + '/pept2lca.txt')
        mqparse.mq_txt.unipept_agg(None, normalized_peptides, unipept_dir)
        self.assertTrue(os.path.exists(unipept_dir + '/pept2lca_family_sc.csv'))

    def test_b_differential_peptides(self):
        infile='testdata/example_mq/txt/analysis/unipept/pept2lca_peptides.csv'
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)
        outfile = outpath + '/peptide_experimental_design.txt'
        quant='Intensity'
        with open(self.config) as f:
            config = yaml.load(f.read(), Loader=Loader)
            design=pd.read_csv('testdata/example_mq/design.csv')
            config = mqparse.mq_txt.update_config(None, config, design, ['E1_Name'])
        mqparse.mq_txt.create_R_parameters(None, config, outfile, quant, 'LEVEL_1')
        design=outfile
        mqparse.mq_txt.diff(None, design, infile, outpath)

        # Check that the files exist - piecharts
        self.assertEqual(os.path.exists(outpath +'/summed_intensity_all.jpeg'), True)
        self.assertEqual(os.path.exists(outpath +'/summed_intensity_After.jpeg'), True)
        self.assertEqual(os.path.exists(outpath +'/summed_intensity_Before.jpeg'), True)

        # Group variance comparisons 
        self.assertEqual(os.path.exists(outpath +'/group_variance'), True)
        self.assertEqual(os.path.exists(outpath +'/group_variance/kw.txt'), True)
        self.assertEqual(os.path.exists(outpath +'/group_variance/group_variance.txt'), True)
        self.assertEqual(os.path.exists(outpath +'/group_variance/dunn_bh.csv'), True)

        # Group limma comparisons
        self.assertEqual(os.path.exists(outpath +'/limma'), True)
        self.assertEqual(os.path.exists(outpath +'/limma/limma_After-Before_intensity.csv'), True)
        self.assertEqual(os.path.exists(outpath +'/limma/limma_After-Before_intensity_qval_0.05.csv'), True)
        self.assertEqual(os.path.exists(outpath +'/limma/limma_After-Before_intensity_qval_0.05_list.txt'), True)
        self.assertEqual(os.path.exists(outpath +'/limma/volcano_After-Before_intensity.jpeg'), True)
        
        
        # Hierarchical clustering of replicates
        self.assertEqual(os.path.exists(outpath +'/replicate_hclust'), True)
        self.assertEqual(os.path.exists(outpath +'/replicate_hclust/replicate_cluster_ward.D2.jpeg'), True)

        # Heatmaps
        self.assertEqual(os.path.exists(outpath +'/heatmaps'), True)
        self.assertEqual(os.path.exists(outpath +'/heatmaps/heatmap_intensity_all.png'), True)
        
        # PCA
        self.assertEqual(os.path.exists(outpath +'/PCA'), True)
        self.assertEqual(os.path.exists(outpath +'/PCA/all_identified_pca.png'), True)
        self.assertEqual(os.path.exists(outpath +'/PCA/all_identified_pca_labelled.png'), True)
    
    
    def test_summarize_diff(self):

        diff='testdata/example_mq/txt/analysis/diff/LEVEL_1/peptide_diff'
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)
        outfile = outpath +'/summary.txt'
        mqparse.mq_txt.summarize_diff(None, diff, outfile)
    
    def test_summarize_gsea(self):
        gsea='testdata/example_mq/txt/analysis/gsea/LEVEL_1/'
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)
        outfile = outpath +'/summary.txt'
        with open(self.config) as f:
            config = yaml.load(f.read(), Loader=Loader)
            design=pd.read_csv('testdata/example_mq/design.csv')
            config = mqparse.mq_txt.update_config(None, config, design, [])
        mqparse.mq_txt.summarize_gsea(None, gsea, outfile, config, 'LEVEL_1')

    def test_b_peptide_normalization(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)
        outfile = outpath + '/peptide_experimental_design.txt'
        quant='Intensity'
        with open(self.config) as f:
            config = yaml.load(f.read(), Loader=Loader)
            design=pd.read_csv('testdata/example_mq/design.csv')
            config = mqparse.mq_txt.update_config(None, config, design, [])
        mqparse.mq_txt.create_R_parameters(None, config, outfile, quant, 'LEVEL_1')
        infile = 'testdata/example_mq/txt/analysis/peptides/target_peptides.txt'
        design = outfile
        normalize ='quantiles'
        impute='knn' # bpca is an options
        mqparse.mq_txt.normalize(None, 'Intensity.', infile, outpath, normalize, impute)
        self.assertEqual(os.path.exists(outpath + '/msnbase/normalized.csv'), True)
        self.assertEqual(os.path.exists(outpath + '/msnbase/normalized.csv.shuffled.csv'), True)
    def test_b_peptide_normalization_quantiles(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)
        outfile = outpath + '/peptide_experimental_design.txt'
        quant='Intensity'
        with open(self.config) as f:
            config = yaml.load(f.read(), Loader=Loader)
            design=pd.read_csv('testdata/example_mq/design.csv')
            config = mqparse.mq_txt.update_config(None, config, design, [])
        mqparse.mq_txt.create_R_parameters(None, config, outfile, quant, 'LEVEL_1')
        infile = 'testdata/example_mq/txt/analysis/peptides/target_peptides.txt'
        design = outfile
        normalize ='quantiles'
        impute='knn'
        mqparse.mq_txt.normalize(None, 'Intensity.', infile, outpath, normalize, impute)
        self.assertEqual(os.path.exists(outpath + '/msnbase/normalized.csv'), True)
        self.assertEqual(os.path.exists(outpath + '/msnbase/normalized.csv.shuffled.csv'), True)

    def test_b_peptide_experimental_design(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)
        outfile = outpath + '/peptide_experimental_design.txt'
        quant='Intensity'
        with open(self.config) as f:
            config = yaml.load(f.read(), Loader=Loader)
            design=pd.read_csv('testdata/example_mq/design.csv')
            config = mqparse.mq_txt.update_config(None, config, design, [])
        mqparse.mq_txt.create_R_parameters(None, config, outfile, quant, 'LEVEL_1')
        self.assertEqual(os.path.exists(outfile), True)

    def test_b_protein_experimental_design(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)
        outfile = outpath + '/protein_experimental_design.txt'
        quant='iBAQ'
        with open(self.config) as f:
            config = yaml.load(f.read(), Loader=Loader)
            design=pd.read_csv('testdata/example_mq/design.csv')
            config = mqparse.mq_txt.update_config(None, config, design, [])
        mqparse.mq_txt.create_R_parameters(None, config, outfile, quant, 'LEVEL_1')
        self.assertEqual(os.path.exists(outfile), True)
    
    def test_b_protein_normalization(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        infile = 'testdata/example_mq/txt/analysis/proteins/target_proteins.tsv'
        design = 'testdata/example_mq/txt/analysis/diff/protein_experimental_design_LEVEL_1.R'
        normalize ='quantiles'
        impute='knn'
        mqparse.mq_txt.normalize(None, 'iBAQ.', infile, outpath, normalize, impute)
        self.assertEqual(os.path.exists(outpath + '/msnbase/normalized.csv'), True)
    
    def test_b_protein_normalization_lfq(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        infile = 'testdata/example_mq/txt/analysis/proteins/target_proteins.tsv'
        design = 'testdata/example_mq/txt/analysis/diff/protein_experimental_design_LEVEL_1.R'
        normalize ='none'
        impute='knn'
        mqparse.mq_txt.normalize(None, 'LFQ.intensity.', infile, outpath, normalize, impute)
        self.assertEqual(os.path.exists(outpath + '/msnbase/normalized.csv'), True)

    def test_b_proteingroup_identifier(self):
        proteins = pd.read_csv('testdata/example_mq/txt/proteinGroups.txt',sep='\t')
        proteins = mqparse.mq_txt.create_protein_group_identifier(None, proteins)
        self.assertIn('Identifier', proteins.columns.tolist())
    
    def test_b_differential_proteins(self):
        design='testdata/example_mq/txt/analysis/diff/protein_experimental_design_LEVEL_1.R'
        infile='testdata/example_mq/txt/analysis/diff/protein_normalization/msnbase/normalized.csv'
        outpath='testdata/example_mq/txt/analysis/testing'
        
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        
        mqparse.mq_txt.diff(None, design, infile, outpath)

        # Check that the files exist - piecharts
        self.assertEqual(os.path.exists(outpath +'/summed_intensity_all.jpeg'), True)
        self.assertEqual(os.path.exists(outpath +'/summed_intensity_After.jpeg'), True)
        self.assertEqual(os.path.exists(outpath +'/summed_intensity_Before.jpeg'), True)

        # Group variance comparisons 
        self.assertEqual(os.path.exists(outpath +'/group_variance'), True)
        self.assertEqual(os.path.exists(outpath +'/group_variance/kw.txt'), True)
        self.assertEqual(os.path.exists(outpath +'/group_variance/group_variance.txt'), True)
        self.assertEqual(os.path.exists(outpath +'/group_variance/dunn_bh.csv'), True)

        # Group limma comparisons
        self.assertEqual(os.path.exists(outpath +'/limma'), True)
        self.assertEqual(os.path.exists(outpath +'/limma/limma_After-Before_intensity.csv'), True)
        self.assertEqual(os.path.exists(outpath +'/limma/limma_After-Before_intensity_qval_0.05.csv'), True)
        self.assertEqual(os.path.exists(outpath +'/limma/limma_After-Before_intensity_qval_0.05_list.txt'), True)
        self.assertEqual(os.path.exists(outpath +'/limma/volcano_After-Before_intensity.jpeg'), True)
        
        res = pd.read_csv(outpath +'/limma/limma_After-Before_intensity_qval_0.05.csv')
        self.assertIn('After', res.columns)
        self.assertIn('Before', res.columns)
        self.assertIn('Exposed.Mean', res.columns)
        self.assertIn('Control.Mean', res.columns)
        print(res.stack())
        
        # Hierarchical clustering of replicates
        self.assertEqual(os.path.exists(outpath +'/replicate_hclust'), True)
        self.assertEqual(os.path.exists(outpath +'/replicate_hclust/replicate_cluster_ward.D2.jpeg'), True)

        # Heatmaps
        self.assertEqual(os.path.exists(outpath +'/heatmaps'), True)
        self.assertEqual(os.path.exists(outpath +'/heatmaps/heatmap_intensity_all.png'), True)
        
        # PCA
        self.assertEqual(os.path.exists(outpath +'/PCA'), True)
        self.assertEqual(os.path.exists(outpath +'/PCA/all_identified_pca.png'), True)
        self.assertEqual(os.path.exists(outpath +'/PCA/all_identified_pca_labelled.png'), True)

    def test_b_differential_proteins_adult_stool(self):
        design  ='testdata/adult_stool_protein_diff/protein_experimental_design_TIMEPOINTS.R'
        infile  ='testdata/adult_stool_protein_diff/normalized.csv'
        outpath ='testdata/adult_stool_protein_diff/diff'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        mqparse.mq_txt.diff(None, design, infile, outpath)
        # Check that the files exist - piecharts
        self.assertEqual(os.path.exists(outpath +'/summed_intensity_all.jpeg'), True)
        self.assertEqual(os.path.exists(outpath +'/summed_intensity_First.jpeg'), True)
        self.assertEqual(os.path.exists(outpath +'/summed_intensity_Second.jpeg'), True)
        # Group variance comparisons 
        self.assertEqual(os.path.exists(outpath +'/group_variance'), True)
        self.assertEqual(os.path.exists(outpath +'/group_variance/kw.txt'), True)
        self.assertEqual(os.path.exists(outpath +'/group_variance/group_variance.txt'), True)
        self.assertEqual(os.path.exists(outpath +'/group_variance/dunn_bh.csv'), True)
        # Group limma comparisons
        self.assertEqual(os.path.exists(outpath +'/limma'), True)
        self.assertEqual(os.path.exists(outpath +'/limma/limma_Second-First_intensity.csv'),True)
        self.assertEqual(os.path.exists(outpath +'/limma/limma_Second-First_intensity_qval_0.05.csv'),True)
        self.assertEqual(os.path.exists(outpath +'/limma/limma_Second-First_intensity_qval_0.05_list.txt'),True)
        self.assertEqual(os.path.exists(outpath +'/limma/volcano_Second-First_intensity.jpeg'),True)
        # Hierarchical clustering of replicates
        self.assertEqual(os.path.exists(outpath +'/replicate_hclust'), True)
        self.assertEqual(os.path.exists(outpath +'/replicate_hclust/replicate_cluster_ward.D2.jpeg'), True)
        # Heatmaps
        self.assertEqual(os.path.exists(outpath +'/heatmaps'), True)
        self.assertEqual(os.path.exists(outpath +'/heatmaps/heatmap_intensity_all.png'), True)
        self.assertEqual(os.path.exists(outpath +'/heatmaps/heatmap_significant.png'), True)

        # PCA
        self.assertEqual(os.path.exists(outpath +'/PCA'), True)
        self.assertEqual(os.path.exists(outpath +'/PCA/all_identified_pca.png'), True)
        self.assertEqual(os.path.exists(outpath +'/PCA/qval_0_05_identified_pca_labelled.png'), True)
        
        with open(outpath + '/limma/limma_Second-First_intensity_qval_0.05_list.txt') as f:
            qval_list = f.read().split('\n')
            qval_list = [ i for i in qval_list if not i == '' ]
            qval_list = [ i for i in qval_list if not i == ' ' ]
        
        self.assertTrue(len(qval_list) > 0 )

    def test_b_protein_id_lists(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)
        with open(self.config) as f:
            config = yaml.load(f.read(), Loader=Loader)
            design=pd.read_csv('testdata/example_mq/design.csv')
            config = mqparse.mq_txt.update_config(None, config, design, [])
        proteingroups = pd.read_csv('testdata/example_mq/txt/analysis/proteins/target_proteins.tsv',sep='\t')
        outfile = outpath +'/protein_ids.txt'
        mqparse.mq_txt.protein_id_lists(None, proteingroups, outfile)
        self.assertEqual(os.path.exists(outfile), True)
    
    def test_b_export_pg_fasta(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)
        outfile = outpath + '/pg.fasta'
        with open(self.config) as f:
            config = yaml.load(f.read(), Loader=Loader)
            design=pd.read_csv('testdata/example_mq/design.csv')
            config = mqparse.mq_txt.update_config(None, config, design, [])
        ids = 'testdata/example_mq/txt/analysis/proteins/protein_ids.txt'
        fasta_file = config['search_fasta']
        mqparse.mq_txt.export_pg_fasta(None, fasta_file, ids, outfile)
        self.assertEqual(os.path.exists(outfile), True)
    
    
    def test_b_export_nr_fasta_proteogenomics(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)
        outfile = outpath + '/nr.fasta'
        fasta_file='testdata/samples_proteogenomics/leading_proteins.fasta'
        mqparse.mq_txt.export_nr_fasta(None, fasta_file, outpath, proteogenomics=True)
        self.assertEqual(os.path.exists(outfile), True)
        self.assertEqual(os.path.exists(outpath + '/id_mapping.json'), True)
    
    def test_b_export_ips_tsv(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)
        outfile = outpath + '/proteins.fasta.tsv'
        infile='testdata/samples_proteogenomics/nr.fasta.tsv'
        mappingfile='testdata/samples_proteogenomics/id_mapping.json'
        mqparse.mq_txt.export_ips_tsv(None, infile, mappingfile,  outfile)
        self.assertEqual(os.path.exists(outfile), True)

    def test_b_export_nr_fasta_not_proteogenomics(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)
        outfile = outpath + '/nr.fasta'
        fasta_file='testdata/samples_proteogenomics/leading_proteins.fasta'
        mqparse.mq_txt.export_nr_fasta(None, fasta_file, outpath, proteogenomics=False)
        self.assertEqual(os.path.exists(outfile), True)
        self.assertEqual(os.path.exists(outpath + '/id_mapping.json'), True)

        #self.assertEqual(os.path.exists(outfile), True)
    def test_b_ips_fasta_with_asterisk(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)

        infile = 'testdata/asterisk.fasta'
        shutil.copyfile(infile, outpath +'/asterisk.fasta')
        mqparse.mq_txt.ips_fasta(None, outpath + '/asterisk.fasta', outpath)
        self.assertEqual(os.path.exists(outpath + '/asterisk.fasta.tsv'), True)

    def test_b_ips_fasta_that_is_failing(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)
        infile = 'testdata/A0A060ZJ24_9ACTN.fasta'
        mqparse.mq_txt.ips_fasta(None, infile, outpath)
        #self.assertEqual(os.path.exists(outfile), True)
    
    def test_b_ips_genesets(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)
        ipr='testdata/example_mq/txt/analysis/fasta/proteins.fasta.tsv'
        proteingroups = pd.read_csv('testdata/example_mq/txt/analysis/proteins/target_proteins.tsv',sep='\t')
        mqparse.mq_txt.ips_genesets(None, ipr, proteingroups, outpath)
        self.assertEqual(os.path.exists(outpath +'/ipr_target_proteins.tsv'),True)
        self.assertEqual(os.path.exists(outpath +'/iprset.Rdata'),True)
        self.assertEqual(os.path.exists(outpath +'/ccset.Rdata'),True)
        self.assertEqual(os.path.exists(outpath +'/bpset.Rdata'),True)
        self.assertEqual(os.path.exists(outpath +'/mfset.Rdata'),True)
        self.assertEqual(os.path.exists(outpath +'/keggset.Rdata'),True)
        self.assertEqual(os.path.exists(outpath +'/kopathwayset.Rdata'),True)
        self.assertEqual(os.path.exists(outpath +'/metacycset.Rdata'),True)
        self.assertEqual(os.path.exists(outpath +'/reactomeset.Rdata'),True)
        self.assertEqual(os.path.exists(outpath +'/kotermset.Rdata'),True)

        # gage.R --outdir $outpath --keggid $kegg_id  || rm -rf ${outpath}/gsea 
    def test_b_custom_genesets(self):
        config='testdata/proteogenomics_sample/testdata/config.yml'
        with open(config) as f:
            config = yaml.load(f.read(), Loader=Loader)
        outpath='testdata/proteogenomics_sample/testdata/temp'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)
        outpath='testdata/proteogenomics_sample/testdata/temp'
        proteingroups = pd.read_csv('testdata/proteogenomics_sample/testdata/target_proteins.tsv',sep='\t')
        proteogenomics=True
        mq_genesets=True
        mqparse.mq_txt.custom_genesets(None, config, proteingroups, outpath, proteogenomics, mq_genesets)
        self.assertEqual(os.path.exists(outpath +'/custom/operons_AllOperons2proteingroups.csv'),True)
        self.assertEqual(os.path.exists(outpath +'/custom/operons_ExponentialPhaseOperons2proteingroups.csv'),True)
        self.assertEqual(os.path.exists(outpath +'/custom/operons_StationaryPhaseOperons2proteingroups.csv'),True)
         
        self.assertEqual(os.path.exists(outpath +'/custom/operons_StationaryPhaseOperons2proteingroups.csv.Rdata'),True)

    def test_b_custom_gsea(self):
        outpath='testdata/proteogenomics_sample/testdata/temp'
        design='testdata/proteogenomics_sample/testdata/protein_experimental_design_StrainCondition.R'
        table='testdata/proteogenomics_sample/testdata/normalized.csv'
        t = pd.read_csv(table)
        
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        shutil.copytree('testdata/proteogenomics_sample/testdata/custom_gsea', outpath) 
        genecol='Mygene.entrez'
        
        kocol='Leading.Protein.Kegg.Orthology.ID'
        t.to_csv(outpath + '/table.csv')
        pval=1000
        mqparse.mq_txt.ips_gsea(None, outpath, outpath +'/StrainCondition',  design, outpath + '/table.csv', genecol, kocol,  pval=pval)
        self.assertEqual(os.path.exists('testdata/proteogenomics_sample/testdata/temp/StrainCondition/S507_ST_S5527_ST/custom/'), True)

    def test_b_ips_gsea(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        design='testdata/example_mq/txt/analysis/diff/protein_experimental_design_LEVEL_1.R'
        table='testdata/example_mq/txt/analysis/diff/protein_normalization/msnbase/normalized.csv'
        t = pd.read_csv(table)
        
        t['Mygene.entrez'] = "5236;85469"
        kocol='Leading.Protein.Kegg.Orthology.ID'
        genecol='Mygene.entrez'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        shutil.copytree('testdata/example_mq/txt/analysis/gsea', outpath) 
        
        t.to_csv(outpath + '/table.csv')
        pval=1000
        mqparse.mq_txt.ips_gsea(None, outpath, outpath +'/LEVEL_1',  design, outpath + '/table.csv', genecol, kocol,  pval=pval)
        self.assertEqual(os.path.exists(outpath +'/LEVEL_1/After_Before/REACTOME.up.other.csv'), True)
    def disabled_test_b_ips_gsea_lfq(self):
        outpath='testdata/example_mq/txt/analysis_lfq/testing'
        design='testdata/example_mq/txt/analysis_lfq/diff/protein_experimental_design_LEVEL_1.R'
        table='testdata/example_mq/txt/analysis_lfq/diff/protein_normalization/msnbase/normalized.csv'
        t = pd.read_csv(table)
        
        t['Mygene.entrez'] = "5236;85469"
        kocol='Leading.Protein.Kegg.Orthology.ID'
        genecol='Mygene.entrez'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        shutil.copytree('testdata/example_mq/txt/analysis_lfq/gsea', outpath) 
        
        t.to_csv(outpath + '/table.csv')
        pval=1000
        mqparse.mq_txt.ips_gsea(None, outpath, outpath +'/LEVEL_1',  design, outpath + '/table.csv', genecol, kocol,  pval=pval)
       #self.assertEqual(os.path.exists(outpath +'/LEVEL_1/After_Before/REACTOME.up.other.csv'), True)
    
    def test_b_ips_gsea_2(self):
        design='testdata/example_mq/txt/analysis/diff//protein_experimental_design_LEVEL_2.R' 
        table='testdata/example_mq/txt/analysis/diff//protein_normalization/msnbase/normalized.csv' 
        genecol='Mygene.entrez' 
        kocol='Leading.Protein.Kegg.Orthology.ID' 
        keggid='hsa' 
        pvalue=1
        outpath='testdata/example_mq/txt/analysis/testing/'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        shutil.copytree('testdata/example_mq/txt/analysis/gsea', outpath) 
        mqparse.mq_txt.ips_gsea(None, outpath, outpath +'/LEVEL_2',  design, table, genecol,kocol,keggid, pval=pvalue)
        #self.assertEqual(os.path.exists(outpath +'/LEVEL_2/After_A_Before_A/REACTOME.up.other.csv'), True)

    def test_b_up2ko_1(self):
        up='P9WN67'
        ko = mqparse.mq_txt.up2ko(None, up)
        self.assertEqual(ko.ko,'K01784')
        self.assertTrue('UDP-glucose 4-epimerase' in ko.name)
        self.assertTrue('mtu00052' in ko.pathways)
        self.assertTrue('ko00052' in ko.ko_pathways)


    def test_b_up2ko_2(self): # No MyGene hits, to come back
        up='A0A2J8Q3M0'
        ko = mqparse.mq_txt.up2ko(None, up)
        self.assertEqual(ko.ko,'K21127')
        self.assertEqual(ko.name,'protein S100-A8') 
    def test_b_up2ko_3(self): # No MyGene hits, to come back
        up='A0A2J8Q3M0_fake'
        ko = mqparse.mq_txt.up2ko(None, up)
        self.assertEqual(ko.ko,'')
        self.assertEqual(ko.name,'') 
    def test_b_up2ko_4(self): # No MyGene hits, to come back
        up='L7N673'
        ko = mqparse.mq_txt.up2ko(None, up)
        self.assertEqual(ko.ko,'')
        self.assertEqual(ko.name,'') 
    
    def test_b_leading_protein_mygene(self):
        proteingroups = pd.read_csv('testdata/example_mq/txt/analysis/proteins/target_proteins.tsv',sep='\t')
        ids = proteingroups['Identifier']
        #self.assertEqual(len(ids), len(set(ids)))
        #proteingroups['Leading Protein']= 'Q05025'
        proteingroups = mqparse.mq_txt.leading_protein_mygene(None, proteingroups.tail())
        cols = proteingroups.columns.tolist()
        print(proteingroups['Mygene.entrez'])
        self.assertIn('Mygene.entrez', cols)
    
    def test_b_leading_protein_gene(self):       
        with open(self.config) as f:
            config = yaml.load(f.read(), Loader=Loader)
        proteingroups = pd.read_csv('testdata/example_mq/txt/analysis/proteins/target_proteins.tsv',sep='\t')
        search_fasta = list(SeqIO.parse(config['search_fasta'],'fasta'))
        reference_fasta = list(SeqIO.parse(config['reference_fasta'],'fasta'))

        ids = proteingroups['Identifier']
        #self.assertEqual(len(ids), len(set(ids)))
        #proteingroups['Leading Protein']= 'Q05025'
        proteingroups = mqparse.mq_txt.leading_protein_gene(None, proteingroups, search_fasta, reference_fasta)
        cols = proteingroups.columns.tolist()
        print(proteingroups['Leading.gene'])
        self.assertIn('Leading.gene', cols)


    def test_b_leading_protein_ko(self):
        proteingroups = pd.read_csv('testdata/example_mq/txt/analysis/proteins/target_proteins.tsv',sep='\t')
        #proteingroups['Leading Protein']= 'Q05025'
        #proteingroups = mqparse.mq_txt.leading_protein_mygene(None, proteingroups)
        #proteingroups = proteingroups[proteingroups['Mygene.entrezgene'].notnull()]
        p = mqparse.mq_txt.leading_protein_ko(None, proteingroups.tail())
        cols = p.columns.tolist()
        self.assertIn('Leading Protein Kegg Orthology ID', cols)
        self.assertIn('Leading Protein Kegg Orthology Name', cols)
        #print(p['Leading Protein Kegg Orthology Name']) 

    def test_b_aggregate_quant(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        table='testdata/example_mq/txt/analysis/diff/protein_normalization/msnbase/normalized.csv'
        design='testdata/example_mq/txt/analysis/diff/protein_experimental_design.R'
        df = pd.read_csv(table)
        agg_col='X_ipr.term.union'
        quant_prefix ='iBAQ.'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        mqparse.mq_txt.aggregate_quant(None, df, agg_col, quant_prefix, outpath)
        self.assertEqual(os.path.exists(outpath +'/' + agg_col + '.csv'), True)

    def test_b_aggregate_quant_ko(self):
        outpath='testdata/example_mq/txt/analysis/testing'
        table='testdata/example_mq/txt/analysis/diff/protein_normalization/msnbase/normalized.csv'
        design='testdata/example_mq/txt/analysis/diff/protein_experimental_design.R'
        df = pd.read_csv(table)
        agg_col='Leading.Protein.Kegg.Orthology'
        quant_prefix ='iBAQ.'
        if os.path.exists(outpath):
            shutil.rmtree(outpath)
        mqparse.mq_txt.aggregate_quant(None, df, agg_col, quant_prefix, outpath)
        self.assertEqual(os.path.exists(outpath +'/' + agg_col + '.csv'), True)
        res = pd.read_csv(outpath +'/' + agg_col + '.csv')

    def test_b_parse_id(self):
        ids = 'tr|A6QNW7|A6QNW7_BOVIN;CON__ENSEMBL:ENSBTAP00000031360'
        correct = 'A6QNW7;CON__ENSEMBL:ENSBTAP00000031360'
        new_ids = mqparse.mq_txt.parse_ids(None, ids)
        self.assertEqual(correct, new_ids)
    
    def test_b_parse_id_proteogenomics(self):
        ids='REV__S507_scaffold1_size371796|S507_scaffold1_size371796_recno_1373.2|(+)343420:345021'
        correct ='REV__S507_scaffold1_size371796_recno_1373.2'
        new_ids = mqparse.mq_txt.parse_ids(None, ids)
        self.assertEqual(correct, new_ids)
    
    def test_c_proteogenomics_proteingroups(self):
        path='testdata/proteogenomics_sample/txt/proteinGroups.txt'
        pg = pd.read_csv(path, sep='\t') 
        config='testdata/proteogenomics_sample/mq_proteogeomics_test.yml'
        with open(config) as f:
            config = yaml.load(f.read(), Loader=Loader)
        config['outdir'] = 'testdata/proteogenomics_sample/test_outpath'
        new_pg =mqparse.mq_txt.proteogenomics_proteingroups(None, pg, config)
        
        # Leading.gene
        # Leading Protein
        # Protein IDs

class Evidence(unittest.TestCase):

    def setUp(self):
        self.evidence = pd.read_csv('testdata/example_mq/txt/evidence.txt', sep='\t', engine='python')
    
    def tearDown(self):
        pass

    def test_load_evidence(self):
        ev = mqparse.Evidence(self.evidence)
    
    def test_export_experiment_peptides(self):
        ev = mqparse.Evidence(self.evidence)
        groups = ['E1','E2','E3', 'E4', 'E5', 'E6', 'E7', 'E8', 'E9', 'E10','E11','E12','E13','E14','E15','E16']
        modifications = ['_(ac)']
        peps = ev.export_peptides(groups, modifications)
        
def main():
    unittest.main(failfast=True)

if __name__ == '__main__':
    main()

